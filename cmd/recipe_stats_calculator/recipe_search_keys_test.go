package main

import (
	recipeStatScanner "hf-data-processing/pkg/recipe_stat_scanner"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_ShouldError_WhenGivenMalformedSearchKeys(t *testing.T) {
	_, validationErr := recipeStatScanner.NewRecipeStatScanner("../../test_fixtures/default.json", "11AM", "1PM", "10120", ",,,,,")

	assert.NotNil(t, validationErr)
}

func Test_ShouldSupport_WhenReceivingEmptyRecipeSearchKeys(t *testing.T) {
	scannerInstance, validationErr := recipeStatScanner.NewRecipeStatScanner("../../test_fixtures/default.json", "11AM", "1PM", "10120", "")
	report, scanningErr := scanAndReport(scannerInstance)

	assert.Nil(t, validationErr)
	assert.Nil(t, scanningErr)
	assert.Exactly(
		t,
		[]string{},
		report.MatchByName,
	)
}

func Test_ShouldReturnCorrectResult_WhenSearchingChicken(t *testing.T) {
	scannerInstance, validationErr := recipeStatScanner.NewRecipeStatScanner("../../test_fixtures/default.json", "11AM", "1PM", "10120", "Chicken")
	report, scanningErr := scanAndReport(scannerInstance)

	assert.Nil(t, validationErr)
	assert.Nil(t, scanningErr)
	assert.ElementsMatch(
		t,
		[]string{
			"Korean-Style Chicken Thighs",
			"Chicken Sausage Pizzas",
			"Honey Sesame Chicken",
			"Creamy Dill Chicken",
			"Spanish One-Pan Chicken",
			"Chicken Pineapple Quesadillas",
			"Cheesy Chicken Enchilada Bake",
			"Hot Honey Barbecue Chicken Legs",
		},
		report.MatchByName,
	)
}

func Test_ShouldReturnCorrectResult_WhenSearchingPork(t *testing.T) {
	scannerInstance, validationErr := recipeStatScanner.NewRecipeStatScanner("../../test_fixtures/default.json", "11AM", "1PM", "10120", "Pork")
	report, scanningErr := scanAndReport(scannerInstance)

	assert.Nil(t, validationErr)
	assert.Nil(t, scanningErr)
	assert.ElementsMatch(
		t,
		[]string{
			"Cajun-Spiced Pulled Pork",
			"Cherry Balsamic Pork Chops",
			"Parmesan-Crusted Pork Tenderloin",
			"Sweet Apple Pork Tenderloin",
			"Hearty Pork Chili",
		},
		report.MatchByName,
	)
}

func Test_ShouldReturnCorrectResult_WhenSearchingInFixture1(t *testing.T) {
	scannerInstance, validationErr := recipeStatScanner.NewRecipeStatScanner("../../test_fixtures/1.json", "11AM", "1PM", "10120", "Steak")
	report, scanningErr := scanAndReport(scannerInstance)

	assert.Nil(t, validationErr)
	assert.Nil(t, scanningErr)
	assert.ElementsMatch(
		t,
		[]string{
			"Speedy Steak Fajitas",
		},
		report.MatchByName,
	)
}

func Test_ShouldNotReturnDups_WhenSearchingInFixture1(t *testing.T) {
	scannerInstance, validationErr := recipeStatScanner.NewRecipeStatScanner("../../test_fixtures/1.json", "11AM", "1PM", "10120", "Steak,Speedy,Fajitas")
	report, scanningErr := scanAndReport(scannerInstance)

	assert.Nil(t, validationErr)
	assert.Nil(t, scanningErr)
	assert.ElementsMatch(
		t,
		[]string{
			"Speedy Steak Fajitas",
		},
		report.MatchByName,
	)
}

func Test_ShouldSupportMultiKeySearch_WhenSearchingInFixture1(t *testing.T) {
	scannerInstance, validationErr := recipeStatScanner.NewRecipeStatScanner("../../test_fixtures/1.json", "11AM", "1PM", "10120", "Creamy,Steak,Cherry,Hot")
	report, scanningErr := scanAndReport(scannerInstance)

	assert.Nil(t, validationErr)
	assert.Nil(t, scanningErr)
	assert.ElementsMatch(
		t,
		[]string{
			"Speedy Steak Fajitas",
			"Cherry Balsamic Pork Chops",
			"Hot Honey Barbecue Chicken Legs",
			"Creamy Dill Chicken",
		},
		report.MatchByName,
	)
}

func Test_ShouldReturnEmpty_WhenSearchingInFixture2(t *testing.T) {
	scannerInstance, validationErr := recipeStatScanner.NewRecipeStatScanner("../../test_fixtures/2.json", "11AM", "1PM", "10120", "Creamy,Steak,Cherry,Hot")
	report, scanningErr := scanAndReport(scannerInstance)

	assert.Nil(t, validationErr)
	assert.Nil(t, scanningErr)
	assert.ElementsMatch(
		t,
		[]string{},
		report.MatchByName,
	)
}
