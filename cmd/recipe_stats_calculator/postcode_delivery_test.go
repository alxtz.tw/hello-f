package main

import (
	recipeStatScanner "hf-data-processing/pkg/recipe_stat_scanner"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_ShouldGiveConsistentPostcodeDeliveryResult_WhenGivenDefaultFixture(t *testing.T) {
	scannerInstance, _ := recipeStatScanner.NewRecipeStatScanner("../../test_fixtures/default.json", "11AM", "1PM", "00000", "")
	report, _ := scanAndReport(scannerInstance)

	assert.Exactly(t, "10176", report.BusiestPostcode.Postcode)
	assert.Exactly(
		t,
		91785,
		report.BusiestPostcode.DeliveryCount,
	)
}

func Test_ShouldGiveCorrectPostcodeDeliveryResult_WhenGivenFixture1(t *testing.T) {
	scannerInstance, _ := recipeStatScanner.NewRecipeStatScanner("../../test_fixtures/1.json", "11AM", "1PM", "00000", "")
	report, _ := scanAndReport(scannerInstance)

	assert.Exactly(t, "10163", report.BusiestPostcode.Postcode)
	assert.Exactly(
		t,
		2,
		report.BusiestPostcode.DeliveryCount,
	)
}

func Test_ShouldGiveCorrectPostcodeDeliveryResult_WhenGivenFixture2(t *testing.T) {
	scannerInstance, _ := recipeStatScanner.NewRecipeStatScanner("../../test_fixtures/2.json", "11AM", "1PM", "00000", "")
	report, _ := scanAndReport(scannerInstance)

	assert.Exactly(t, "", report.BusiestPostcode.Postcode)
	assert.Exactly(
		t,
		0,
		report.BusiestPostcode.DeliveryCount,
	)
}

func Test_ShouldGiveCorrectPostcodeDeliveryResult_WhenGivenFixture3(t *testing.T) {

	scannerInstance, _ := recipeStatScanner.NewRecipeStatScanner("../../test_fixtures/3.json", "11AM", "1PM", "00000", "")
	report, _ := scanAndReport(scannerInstance)

	assert.Exactly(t, "10186", report.BusiestPostcode.Postcode)
	assert.Exactly(
		t,
		7,
		report.BusiestPostcode.DeliveryCount,
	)
}
