package main

import (
	recipeStatScanner "hf-data-processing/pkg/recipe_stat_scanner"
	"testing"

	"github.com/stretchr/testify/assert"
)

// NOTE: need the test the following
// (not exist) 0AM
// (not exist) 12AM
// (not exist) 0PM
// (not exist) 12PM
// (exist) 1AM
// (exist) 11PM
// (exist) 1PM

func Test_ShouldGiveConsistentDeliveryTrafficResult_WhenGivenDefaultFixture(t *testing.T) {
	scannerInstance, validationErr := recipeStatScanner.NewRecipeStatScanner("../../test_fixtures/default.json", "11AM", "1PM", "10120", "")
	report, scanningErr := scanAndReport(scannerInstance)

	assert.Nil(t, validationErr)
	assert.Nil(t, scanningErr)
	assert.Exactly(
		t,
		725,
		report.CountPerPostcodeAndTime.DeliveryCount,
	)
}

func Test_ShouldGiveConsistentDeliveryTrafficResult_WhenGivenDefaultFixtureWithNoMatchingRecords(t *testing.T) {
	scannerInstance, validationErr := recipeStatScanner.NewRecipeStatScanner("../../test_fixtures/default.json", "1PM", "2PM", "10120", "")
	report, scanningErr := scanAndReport(scannerInstance)

	assert.Nil(t, validationErr)
	assert.Nil(t, scanningErr)
	assert.Exactly(
		t,
		0,
		report.CountPerPostcodeAndTime.DeliveryCount,
	)
}

func Test_ShouldGiveConsistentDeliveryTrafficResult_WhenGivenDefaultFixtureWithAllRecords(t *testing.T) {
	scannerInstance, validationErr := recipeStatScanner.NewRecipeStatScanner("../../test_fixtures/default.json", "1AM", "11PM", "90982", "")
	report, scanningErr := scanAndReport(scannerInstance)

	assert.Nil(t, validationErr)
	assert.Nil(t, scanningErr)
	assert.Exactly(
		t,
		0,
		report.CountPerPostcodeAndTime.DeliveryCount,
	)
}

func Test_ShouldReturnValidationError_WhenGivenMaxMinTimeRange(t *testing.T) {
	_, validationErr := recipeStatScanner.NewRecipeStatScanner("../../test_fixtures/default.json", "11PM", "1AM", "90982", "")
	assert.NotNil(t, validationErr)
}

func Test_ShouldReturnValidationError_WhenGivenZeroOrTwelveHrDigit(t *testing.T) {
	_, validationErr := recipeStatScanner.NewRecipeStatScanner("../../test_fixtures/default.json", "0AM", "12PM", "10120", "")
	assert.NotNil(t, validationErr)
}

func Test_ShouldReturnValidationError_WhenGivenUnexpectedDeliveryFormat(t *testing.T) {
	scannerInstance, _ := recipeStatScanner.NewRecipeStatScanner("../../test_fixtures/err_delivery_format.json", "1AM", "11PM", "10120", "")
	_, scanningErr := scanAndReport(scannerInstance)

	assert.NotNil(t, scanningErr)
}

func Test_ShouldGiveCorrectDeliveryTrafficResult_WhenGivenFixture1(t *testing.T) {
	scannerInstance, validationErr := recipeStatScanner.NewRecipeStatScanner("../../test_fixtures/1.json", "11AM", "2PM", "10163", "")
	report, scanningErr := scanAndReport(scannerInstance)

	assert.Nil(t, validationErr)
	assert.Nil(t, scanningErr)
	assert.Exactly(
		t,
		1,
		report.CountPerPostcodeAndTime.DeliveryCount,
	)
}

func Test_ShouldGiveCorrectDeliveryTrafficResult_WhenGivenFixture2(t *testing.T) {
	scannerInstance, validationErr := recipeStatScanner.NewRecipeStatScanner("../../test_fixtures/2.json", "11AM", "2PM", "00000", "")
	report, scanningErr := scanAndReport(scannerInstance)

	assert.Nil(t, validationErr)
	assert.Nil(t, scanningErr)
	assert.Exactly(
		t,
		0,
		report.CountPerPostcodeAndTime.DeliveryCount,
	)
}

func Test_ShouldGiveCorrectDeliveryTrafficResult_WhenGivenFixture3(t *testing.T) {
	scannerInstance, validationErr := recipeStatScanner.NewRecipeStatScanner("../../test_fixtures/3.json", "6AM", "6PM", "10163", "")
	report, scanningErr := scanAndReport(scannerInstance)

	assert.Nil(t, validationErr)
	assert.Nil(t, scanningErr)
	assert.Exactly(
		t,
		5,
		report.CountPerPostcodeAndTime.DeliveryCount,
	)
}
