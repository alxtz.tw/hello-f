package main

import (
	recipeStatScanner "hf-data-processing/pkg/recipe_stat_scanner"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_ShouldGiveConsistentResult_WhenGivenDefaultFixture(t *testing.T) {
	scannerInstance, _ := recipeStatScanner.NewRecipeStatScanner("../../test_fixtures/default.json", "11AM", "1PM", "00000", "")
	report, _ := scanAndReport(scannerInstance)

	assert.Exactly(t, 29, report.UniqueRecipeCount)
	assert.Exactly(
		t,
		map[string]int{
			"Cajun-Spiced Pulled Pork":           667365,
			"Cheesy Chicken Enchilada Bake":      333012,
			"Cherry Balsamic Pork Chops":         333889,
			"Chicken Pineapple Quesadillas":      331514,
			"Chicken Sausage Pizzas":             333306,
			"Creamy Dill Chicken":                333103,
			"Creamy Shrimp Tagliatelle":          333395,
			"Crispy Cheddar Frico Cheeseburgers": 333251,
			"Garden Quesadillas":                 333284,
			"Garlic Herb Butter Steak":           333649,
			"Grilled Cheese and Veggie Jumble":   333742,
			"Hearty Pork Chili":                  333355,
			"Honey Sesame Chicken":               333748,
			"Hot Honey Barbecue Chicken Legs":    334409,
			"Korean-Style Chicken Thighs":        333069,
			"Meatloaf à La Mom":                  333570,
			"Mediterranean Baked Veggies":        332939,
			"Melty Monterey Jack Burgers":        333264,
			"Mole-Spiced Beef Tacos":             332993,
			"One-Pan Orzo Italiano":              333109,
			"Parmesan-Crusted Pork Tenderloin":   333311,
			"Spanish One-Pan Chicken":            333291,
			"Speedy Steak Fajitas":               333578,
			"Spinach Artichoke Pasta Bake":       333545,
			"Steakhouse-Style New York Strip":    333473,
			"Stovetop Mac 'N' Cheese":            333098,
			"Sweet Apple Pork Tenderloin":        332595,
			"Tex-Mex Tilapia":                    333749,
			"Yellow Squash Flatbreads":           333394,
		},
		scannerInstance.RecipeCountMap,
	)
}

func Test_ShouldGiveCorrectResult_WhenGivenFixture1(t *testing.T) {
	scannerInstance, _ := recipeStatScanner.NewRecipeStatScanner("../../test_fixtures/1.json", "11AM", "1PM", "00000", "")
	report, _ := scanAndReport(scannerInstance)

	assert.Exactly(t, 4, report.UniqueRecipeCount)
	assert.Exactly(
		t,
		map[string]int{
			"Creamy Dill Chicken":             1,
			"Speedy Steak Fajitas":            1,
			"Cherry Balsamic Pork Chops":      2,
			"Hot Honey Barbecue Chicken Legs": 1,
		},
		scannerInstance.RecipeCountMap,
	)
}

func Test_ShouldGiveCorrectResult_WhenGivenFixture2(t *testing.T) {
	scannerInstance, _ := recipeStatScanner.NewRecipeStatScanner("../../test_fixtures/2.json", "11AM", "1PM", "00000", "")
	report, _ := scanAndReport(scannerInstance)

	assert.Exactly(t, 0, report.UniqueRecipeCount)
	assert.Exactly(
		t,
		map[string]int{},
		scannerInstance.RecipeCountMap,
	)
}

func Test_ShouldGiveCorrectResult_WhenGivenFixture3(t *testing.T) {
	scannerInstance, _ := recipeStatScanner.NewRecipeStatScanner("../../test_fixtures/3.json", "11AM", "1PM", "00000", "")
	report, _ := scanAndReport(scannerInstance)

	assert.Exactly(t, 2, report.UniqueRecipeCount)
	assert.Exactly(
		t,
		map[string]int{
			"Cherry Balsamic Pork Chops":      6,
			"Hot Honey Barbecue Chicken Legs": 6,
		},
		scannerInstance.RecipeCountMap,
	)
}

func Test_ShouldEndGracefully_WhenGivenBrokenFixture(t *testing.T) {
}

func Test_ShouldEndGracefully_WhenGivenFileNotFound(t *testing.T) {
}

func Test_ShouldWarn_WhenReadingOversizedFile(t *testing.T) {
}

func Test_ShouldBeUnexpected_WhenGivenFileWithAllUniquePostcode(t *testing.T) {
}

func Test_ShouldBeUnexpected_WhenGivenFileWith100mRows(t *testing.T) {
}
