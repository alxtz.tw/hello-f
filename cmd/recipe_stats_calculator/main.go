package main

import (
	"encoding/json"
	"flag"
	"fmt"
	recipeStatScanner "hf-data-processing/pkg/recipe_stat_scanner"
	"os"
)

func main() {
	paramFileName := flag.String("file", "", "(required) (example: /Users/alxtz/Desktop/-HFtest-backend-go-phpalxtz/test_fixtures/default.json)")
	paramRecipeSearchKeys := flag.String("keys", "", "(required) (example: Chicken,Cheese,Pork)")
	paramTrafficSearchPostcode := flag.String("postcode", "", "(required) (example: 10120)")
	paramTrafficSearchTimeFrom := flag.String("from", "", "(required) (example: 1AM)")
	paramTrafficSearchTimeTo := flag.String("to", "", "(required) (example: 11PM)")

	flag.Parse()
	if *paramFileName == "" || *paramRecipeSearchKeys == "" || *paramTrafficSearchPostcode == "" || *paramTrafficSearchTimeFrom == "" || *paramTrafficSearchTimeTo == "" {
		fmt.Println("All parameters are required.")
		flag.Usage()
		os.Exit(1)
	}

	scannerInstance, validationErr := recipeStatScanner.NewRecipeStatScanner(
		*paramFileName,
		*paramTrafficSearchTimeFrom,
		*paramTrafficSearchTimeTo,
		*paramTrafficSearchPostcode,
		*paramRecipeSearchKeys,
	)

	if validationErr != nil {
		fmt.Println(validationErr)
		os.Exit(1)
	}

	report, scanningErr := scanAndReport(scannerInstance)

	if scanningErr != nil {
		fmt.Println(scanningErr)
		os.Exit(1)
	}

	jsonReport, _ := json.MarshalIndent(report, "", "    ")
	fmt.Println(string(jsonReport))
}

func scanAndReport(scannerInstance *recipeStatScanner.RecipeStatScanner) (report recipeStatScanner.Report, err error) {
	readLineCount := 0
	valPostcode := ""
	valRecipe := ""
	valDelivery := ""

	var scanningErr error

	scannerInstance.ScanAll(func(rawLine string) error {
		key, val, err := recipeStatScanner.ExtractKV(rawLine)

		if err != nil {
			return err
		}

		readLineCount++

		switch key {
		case "postcode":
			valPostcode = val
		case "recipe":
			valRecipe = val
		case "delivery":
			valDelivery = val
		}

		if readLineCount%3 == 0 {
			scannerInstance.PostcodeDeliveryMap[valPostcode]++
			scannerInstance.RecipeCountMap[valRecipe]++

			var recordFromInt, recordToInt int

			if recordFromInt, recordToInt, err = recipeStatScanner.ValidateDeliveryStr(valDelivery); err != nil {
				scanningErr = err
				return err
			}

			if scannerInstance.ParamTrafficSearchTimeFrom <= recordFromInt &&
				scannerInstance.ParamTrafficSearchTimeTo >= recordToInt &&
				scannerInstance.ParamTrafficSearchPostcode == valPostcode {
				scannerInstance.SearchDeliveryCount++
			}
		}

		return nil
	})

	if scanningErr != nil {
		return recipeStatScanner.Report{}, scanningErr
	}

	return scannerInstance.Report(), nil
}
