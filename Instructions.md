### Do These First
- If you wanna run the tests locally, decompress `<project>test_fixtures/default.tar.gz` rename the content into `<project>/test_fixtures/default.json` (this file was way to big to host on github)

### Run Tests
```
go test -v ./cmd/recipe_stats_calculator
```

### Profiling
```
go test -v ./cmd/recipe_stats_calculator -run ^Test_ShouldGiveConsistentResult_WhenGivenDefaultFixture$ -bench=. -benchmem -memprofile artifacts/memprofile.out -cpuprofile artifacts/profile.out
```

```
go tool pprof artifacts/profile.out
# (pprof) web
```

### Execute Locally

```
go run cmd/recipe_stats_calculator/main.go -file="<project absolute path>/default.json" -keys="Chicken,Cheese,Pork" -postcode="12345" -from="2AM" -to="10PM"
```

### Build and Run the CLI Locally

```
go build -o ./artifacts cmd/recipe_stats_calculator/main.go

./artifacts/main -file="/Users/alxtz/Desktop/-HFtest-backend-go-phpalxtz/test_fixtures/default.json" -from="1AM" -keys="Chicken,Cheese,Pork" -postcode="10120" -to="11PM"
```

### Build and Execute with Docker

```
docker build -t tmp-image .

docker run -v <project absolute path>/test_fixtures/default.json:/default.json tmp-image -from="1AM" -keys="Chicken,Cheese,Pork" -postcode="10120" -to="11PM" -file="/default.json"
```