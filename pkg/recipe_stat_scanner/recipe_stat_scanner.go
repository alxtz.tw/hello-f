package recipe_stat_scanner

import (
	"bufio"
	"fmt"
	"os"
	"slices"
	"strings"
)

type RecipeStatScanner struct {
	paramFilename              string
	ParamTrafficSearchPostcode string
	ParamTrafficSearchTimeFrom int
	ParamTrafficSearchTimeTo   int
	ParamRecipeSearchKeys      []string

	RecipeCountMap      map[string]int // recipe name -> occurrence count
	PostcodeDeliveryMap map[string]int // postcode -> delivery count
	SearchDeliveryCount int
}

func NewRecipeStatScanner(
	fileName string,
	trafficSearchTimeFrom string,
	trafficSearchTimeTo string,
	trafficSearchPostcode string,
	recipeSearchKeys string,
) (*RecipeStatScanner, error) {
	var givenFromInt, givenToInt int
	var err error

	if givenFromInt, err = ValidateTimeStr(trafficSearchTimeFrom); err != nil {
		return nil, err
	}

	if givenToInt, err = ValidateTimeStr(trafficSearchTimeTo); err != nil {
		return nil, err
	}

	if givenFromInt >= givenToInt {
		return nil, fmt.Errorf("invalid time range found: %s - %s", trafficSearchTimeFrom, trafficSearchTimeTo)
	}

	var searchKeys []string
	if len(recipeSearchKeys) == 0 {
		searchKeys = []string{}
	} else {
		searchKeys = strings.Split(recipeSearchKeys, ",")
		if len(searchKeys) > 0 && slices.Index(searchKeys, "") != -1 {
			return nil, fmt.Errorf("invalid empty recipe search keys found")
		}
	}

	return &RecipeStatScanner{
		paramFilename:              fileName,
		ParamTrafficSearchPostcode: trafficSearchPostcode,
		ParamTrafficSearchTimeFrom: givenFromInt,
		ParamTrafficSearchTimeTo:   givenToInt,

		RecipeCountMap:        make(map[string]int),
		PostcodeDeliveryMap:   make(map[string]int),
		ParamRecipeSearchKeys: searchKeys,
	}, nil
}

type ScanFunc func(rawLine string) error

func (s *RecipeStatScanner) ScanAll(scanFunc ScanFunc) {
	jsonFile, err := os.Open(s.paramFilename)

	if err != nil {
		fmt.Println("Error opening JSON file:", err)
	} else {
		defer jsonFile.Close()
	}

	bufScanner := bufio.NewScanner(jsonFile)

	var lineNumber = -1

	for bufScanner.Scan() {
		lineNumber++

		// NOTE: for capturing postcode (2nd line), recipe (3rd line) and delivery time (4th line)
		if lineNumber%5 >= 2 && lineNumber%5 <= 4 {
			err := scanFunc(bufScanner.Text())
			if err != nil {
				break
			}
		}
	}
}

func ExtractKV(input string) (key string, value string, err error) {
	var quoteIndexes []int

	for i, char := range input {
		if char == '"' {
			quoteIndexes = append(quoteIndexes, i)
		}

	}

	if len(quoteIndexes) < 4 {
		err = fmt.Errorf("unexpected property format")
	} else {
		key = input[quoteIndexes[0]+1 : quoteIndexes[1]]
		value = input[quoteIndexes[2]+1 : quoteIndexes[len(quoteIndexes)-1]]

		return key, value, nil
	}

	if err != nil {
		return "", "", err
	} else {
		return "", "", fmt.Errorf("unexpected error")
	}
}
