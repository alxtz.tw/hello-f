package recipe_stat_scanner

import (
	"fmt"
	"slices"
	"strconv"
	"strings"
)

func ValidateDeliveryStr(delivery string) (from int, to int, err error) {
	split1 := strings.Split(delivery, " - ")
	head, toStr := split1[0], split1[1]

	split2 := strings.Split(head, " ")
	weekdayStr, fromStr := split2[0], split2[1]

	if err = ValidateWeekdayStr(weekdayStr); err != nil {
		return 0, 0, err
	}

	var toInt, fromInt int

	if toInt, err = ValidateTimeStr(toStr); err != nil {
		return 0, 0, err
	}

	if fromInt, err = ValidateTimeStr(fromStr); err != nil {
		return 0, 0, err
	}

	if fromInt >= toInt {
		return 0, 0, fmt.Errorf("invalid time range found: %s", delivery)
	}

	return fromInt, toInt, nil
}

func ValidateTimeStr(time string) (toInt int, err error) {
	if len(time) < 3 {
		return 0, fmt.Errorf("invalid time format found: %s", time)
	}

	digitStr, symbol := time[:len(time)-2], time[len(time)-2:]

	if symbol != "AM" && symbol != "PM" {
		return 0, fmt.Errorf("invalid time format found: %s", time)
	}

	digitInt, err := strconv.Atoi(digitStr)

	if err != nil {
		return 0, fmt.Errorf("invalid time format found: %s", time)
	}

	if digitInt < 1 || digitInt > 11 {
		return 0, fmt.Errorf("invalid time format found: %s", time)
	}

	if symbol == "PM" {
		return digitInt + 12, nil
	} else {
		return digitInt, nil
	}
}

func ValidateWeekdayStr(weekday string) (err error) {
	if !slices.Contains([]string{"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"}, weekday) {
		return fmt.Errorf("invalid weekday format found: %s", weekday)
	}

	return nil
}
