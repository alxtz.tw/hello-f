package recipe_stat_scanner

import (
	"fmt"
	"slices"
	"strings"
)

type RecipeCount struct {
	Recipe string `json:"recipe"`
	Count  int    `json:"count"`
}

type CountPerPostcodeAndTime struct {
	Postcode      string `json:"postcode"`
	From          string `json:"from"`
	To            string `json:"to"`
	DeliveryCount int    `json:"delivery_count"`
}

type Report struct {
	UniqueRecipeCount int           `json:"unique_recipe_count"`
	CountPerRecipe    []RecipeCount `json:"count_per_recipe"`
	BusiestPostcode   struct {
		Postcode      string `json:"postcode"`
		DeliveryCount int    `json:"delivery_count"`
	} `json:"busiest_postcode"`
	CountPerPostcodeAndTime CountPerPostcodeAndTime `json:"count_per_postcode_and_time"`
	MatchByName             []string                `json:"match_by_name"`
}

func (s *RecipeStatScanner) Report() (report Report) {
	report.UniqueRecipeCount = len(s.RecipeCountMap)

	for recipeName, count := range s.RecipeCountMap {
		report.CountPerRecipe = append(report.CountPerRecipe, RecipeCount{Recipe: recipeName, Count: count})
	}

	slices.SortFunc(report.CountPerRecipe, func(a, b RecipeCount) int {
		return strings.Compare(a.Recipe, b.Recipe)
	})

	var maxKey string = ""

	for key := range s.PostcodeDeliveryMap {
		if maxKey == "" {
			maxKey = key
		} else {
			if s.PostcodeDeliveryMap[maxKey] < s.PostcodeDeliveryMap[key] {
				maxKey = key
			}
		}
	}

	report.BusiestPostcode.Postcode = maxKey
	report.BusiestPostcode.DeliveryCount = s.PostcodeDeliveryMap[maxKey]

	report.CountPerPostcodeAndTime.Postcode = s.ParamTrafficSearchPostcode
	report.CountPerPostcodeAndTime.From = fmt.Sprintf("%dAM", s.ParamTrafficSearchTimeFrom)
	report.CountPerPostcodeAndTime.To = fmt.Sprintf("%dPM", s.ParamTrafficSearchTimeTo-12)
	report.CountPerPostcodeAndTime.DeliveryCount = s.SearchDeliveryCount

	var matches []string = []string{}

	for recipeName := range s.RecipeCountMap {
		for _, searchKey := range s.ParamRecipeSearchKeys {
			if strings.Contains(recipeName, searchKey) {
				matches = append(matches, recipeName)
				break
			}
		}
	}

	report.MatchByName = matches

	slices.SortFunc(report.MatchByName, func(a, b string) int {
		return strings.Compare(a, b)
	})

	return report
}
