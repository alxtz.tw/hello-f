FROM golang:1.21.5 AS builder
WORKDIR /app
COPY go.mod go.sum ./
RUN go mod download
COPY . ./

RUN CGO_ENABLED=0 GOOS=linux go build -o /app/artifacts /app/cmd/recipe_stats_calculator/main.go

FROM gcr.io/distroless/base-debian11
COPY --from=builder /app/artifacts/main /main
ENTRYPOINT ["/main"]
